module "vpc"{
  source = "./module"
  name_prefix             = "jlr-cloudcarneo-vpc"
  cidr                    = "172.16.0.0/16"
  secondary_cidr_blocks   = ["172.17.0.0/16"]
  enable_nat_gateway      = true
  single_nat_gateway      = true
  create_pod_subnets      = true
  create_vpc_endpoints    = true
  #create_database_subnets = true

  # length of azs and subnets should be equal
  region                       = "eu-west-2"
  azs                          = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
  private_subnets              = ["172.16.0.0/20", "172.16.16.0/20", "172.16.32.0/20"]
  public_subnets               = ["172.16.48.0/22", "172.16.52.0/22", "172.16.56.0/22"]
  #database_subnets             = ["172.17.192.0/20", "172.17.208.0/20", "172.17.224.0/20"]
  pod_subnets                  = ["172.17.0.0/18", "172.17.64.0/18", "172.17.128.0/18"]
  
  private_subnet_tags ={
    "kubernetes.io/role/internal-elb" = 1
    "kubernetes.io/cluster/cloudcarneo" = ""
  }
  public_subnet_tags ={
    "kubernetes.io/role/elb" = 1
    "kubernetes.io/cluster/cloudcarneo" = ""
  }
}


