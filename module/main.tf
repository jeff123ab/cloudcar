#######
# VPC #
#######
module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.11.5"

  name                           = var.name_prefix
  cidr                           = var.cidr
  secondary_cidr_blocks          = var.create_pod_subnets == true ? var.secondary_cidr_blocks : []
  azs                            = var.azs
  private_subnets                = var.private_subnets
  public_subnets                 = var.public_subnets
  #database_subnets               = var.create_database_subnets == true ? var.secondary_cidr_blocks : []
  #create_database_subnet_group   = var.create_database_subnet_group
  enable_dns_hostnames           = var.enable_dns_hostnames
  enable_dns_support             = var.enable_dns_support
  enable_ipv6                    = var.enable_ipv6
  enable_nat_gateway             = var.enable_nat_gateway
  single_nat_gateway             = var.single_nat_gateway
  manage_default_security_group  = var.manage_default_security_group
  default_security_group_ingress = var.default_security_group_ingress
  default_security_group_egress  = var.default_security_group_egress

  tags = merge(var.tags, {
    owner       = "SRE"
    environment = var.name_prefix
    terraform   = true
  })

  public_subnet_tags  = merge(var.public_subnet_tags, { Type = "public" })
  private_subnet_tags = merge(var.private_subnet_tags, { Type = "private" })

}



##########################
# Pod networking subnets #
##########################

resource "aws_subnet" "pods" {
  count             = (var.create_pod_subnets == true) ? length(var.azs) : 0
  cidr_block        = element(var.pod_subnets, count.index)
  vpc_id            = module.vpc.vpc_id
  availability_zone = element(var.azs, count.index)

  tags = {
    "Name" = "${var.name_prefix}-pods-${element(var.azs, count.index)}"
    "Type" = "pods"
  }

  depends_on = [module.vpc.vpc_secondary_cidr_blocks]
}

resource "aws_route_table" "pod_route_table" {
  count  = (var.create_pod_subnets == true) ? length(var.azs) : 0
  vpc_id = module.vpc.vpc_id

  tags = {
    Name = "${var.name_prefix}-pods-${element(var.azs, count.index)}"
  }
}

resource "aws_route_table_association" "pod_route_association" {
  count          = (var.create_pod_subnets == true) ? length(aws_route_table.pod_route_table.*.id) : 0
  route_table_id = element(aws_route_table.pod_route_table.*.id, count.index)
  subnet_id      = element(aws_subnet.pods.*.id, count.index)
}



####################
# Database subnets #
####################

// resource "aws_subnet" "db_subnets" {
//   count             = (var.create_database_subnets == true) ? length(var.azs) : 0
//   cidr_block        = element(var.database_subnets, count.index)
//   vpc_id            = module.vpc.vpc_id
//   availability_zone = element(var.azs, count.index)

//   tags = {
//     "Name" = "${var.name_prefix}-db-${element(var.azs, count.index)}"
//     "Type" = "db"
//   }

//   depends_on = [module.vpc.vpc_secondary_cidr_blocks]
// }

// resource "aws_route_table" "db_route_table" {
//   count  = (var.create_database_subnets == true) ? length(var.azs) : 0
//   vpc_id = module.vpc.vpc_id

//   tags = {
//     Name = "${var.name_prefix}-db-${element(var.azs, count.index)}"
//   }
// }

// resource "aws_route_table_association" "db_route_association" {
//   count          = (var.create_database_subnets == true) ? length(aws_route_table.db_route_table.*.id) : 0
//   route_table_id = element(aws_route_table.db_route_table.*.id, count.index)
//   subnet_id      = element(aws_subnet.db_subnets.*.id, count.index)
// }

   
#####################
# For VPC endpoints #
#####################
module "vpc_endpoint_sg" {
  count   = var.create_vpc_endpoints == true ? 1 : 0
  source  = "terraform-aws-modules/security-group/aws"
  version = "4.8.0"
  name    = "${var.name_prefix}-endpoints"

  vpc_id              = module.vpc.vpc_id
  description         = "Allow access to VPC endpoints from VPC."
  use_name_prefix     = true # Should be true to be able to update security group name after initial creation
  ingress_cidr_blocks = concat([module.vpc.vpc_cidr_block], var.secondary_cidr_blocks)
  ingress_rules       = ["all-all"]
  egress_cidr_blocks  = ["0.0.0.0/0"]
  egress_rules        = ["all-all"]
  tags                = merge(var.tags, { Name = "${var.name_prefix}-endpoints" })
}

module "vpc_endpoints" {
  count   = var.create_vpc_endpoints == true ? 1 : 0
  source  = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"
  version = "3.11.5"

  vpc_id             = module.vpc.vpc_id
  security_group_ids = module.vpc_endpoint_sg.*.security_group_id

  endpoints = {
    s3 = {
      service = "s3"
      tags    = { Name = "s3-vpc-endpoint" }
    },
    ssm = {
      service             = "ssm"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
    },
    ssmmessages = {
      service             = "ssmmessages"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
    },
    ec2 = {
      service             = "ec2"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
    },
    ec2messages = {
      service             = "ec2messages"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
    },
    kms = {
      service             = "kms"
      private_dns_enabled = true
      subnet_ids          = module.vpc.private_subnets
    }
  }

  tags = merge(var.tags, {
    owner       = "SRE"
    environment = var.name_prefix
    terraform   = true
  })
}
