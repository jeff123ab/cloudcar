variable "name_prefix" {
  type        = string
  description = "A name prefix to use in resource names."
}

variable "tags" {
  type        = map(any)
  description = "Additional tags to add to resources."
  default     = {}
}

variable "region" {
  type        = string
  description = "The AWS region."
}

variable "cidr" {
  type        = string
  description = "The VPC CIDR."
}

variable "azs" {
  type        = list(string)
  description = "A list of availability zones in which to create subnets."
}

variable "private_subnets" {
  type        = list(string)
  description = "A list of CIDRs for the private subnets. Length must match the length of 'azs'"
}

variable "private_subnet_tags" {
  type        = map(any)
  description = "Tags to apply to private subnets."
  default     = {}
}

variable "public_subnets" {
  type        = list(string)
  description = "A list of CIDRs for the public subnets. Length must match the length of 'azs'"
}

variable "public_subnet_tags" {
  type        = map(any)
  description = "Tags to apply to private subnets."
  default     = {}
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "Enable DNS hostnames in the VPC."
  default     = true
}

variable "enable_dns_support" {
  type        = bool
  description = "Enable DNS support in the VPC"
  default     = true
}

variable "enable_ipv6" {
  type        = bool
  description = "Enable IPv6 support in the VPC"
  default     = false
}

variable "enable_nat_gateway" {
  type        = bool
  description = "Create a NAT gateway."
  default     = true
}

variable "single_nat_gateway" {
  type        = bool
  description = "Create a single NAT gateway for all private subnets."
  default     = false
}

variable "manage_default_security_group" {
  type        = bool
  description = "Managed the default security group for the VPC."
  default     = true
}

variable "default_security_group_ingress" {
  type        = list(map(any))
  description = "A list of ingress rules to create if managing the default security group."
  default     = []
}

variable "default_security_group_egress" {
  type        = list(map(any))
  description = "A list of egress rules to create if managing the default security group."
  default     = []
}



variable "create_pod_subnets" {
    type        = bool
    description = "Create subnets dedicated for Pod networking."
    default     = false
  }

variable "secondary_cidr_blocks" {
    type        = list(string)
    description = "A list of secondary CIDR blocks to add to the VPC."
    default     = []
  }

variable "pod_subnets" {
   type        = list(string)
   description = "A list of Pod subnet CIDR blocks."
   default     = []
  }

 variable "create_vpc_endpoints" {
   type        = bool
   description = "Create VPC endpoints in the VPC."
   default     = false
 }

//  variable "create_database_subnets" {
//    type        = bool
//    description = "Create subnets specifically for databases."
//    default     = false
//   }

// variable "database_subnets" {
//   type        = list(string)
//    description = "A list of database subnet CIDR blocks."
//    default     = []
// }

// variable "create_database_subnet_group" {
//    type        = bool
//    description = "create a database security group"
//    default     = false
//}


