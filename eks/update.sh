#!/bin/bash
echo "installing dependencies------------------------------------------------------------------------------------------------"
apt-get update
apt-get install curl -y
apt-get install tar -y
apt-get install unzip -y
apt-get install sudo -y
echo "installing eksctl------------------------------------------------------------------------------------------------"
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
mv /tmp/eksctl /usr/local/bin
eksctl version
echo "installing aws cli------------------------------------------------------------------------------------------------"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
aws --version
export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
echo "installing Kubectl------------------------------------------------------------------------------------------------"
curl -o kubectl https://s3.us-west-2.amazonaws.com/amazon-eks/1.22.6/2022-03-09/bin/linux/amd64/kubectl
chmod +x ./kubectl
mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin
kubectl version --short --client


cd ./eks 
mkdir ~/.kube/
cat $KUBECONFIG | base64 -d > config
mv config ~/.kube/

kubectl get namespaces
